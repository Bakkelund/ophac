# ophac -- Order Preserving Hierarachical Agglomerative Clustering
The code in this project realises the theory described in <https://arxiv.org/abs/2004.12488>.
The functionality provided is that of _**order preserving hierarchical agglomerative clustering of partially ordered sets**_.

The [ophac wiki](https://bitbucket.org/Bakkelund/ophac/wiki/Home) provides examples of how to use the library.

## Licensing

The software in this package is released under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.en.html).

## Platform requirements
`ophac` runs on **python 3.0** or greater, and makes use of the following python libraries:

* numpy
* scipy
* [ophac-cpp](https://pypi.org/project/ophac-cpp/)

## Installation

```python
python -m pip install ophac [--user]
```

## Source

The full source is available from
<https://bitbucket.org/Bakkelund/ophac/src/v04/>.